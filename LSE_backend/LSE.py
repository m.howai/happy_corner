from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
from main import *
from datetime import datetime
import time

main_object = LSE_code_isin()
ld_detail_delisted,ld_detail_new = main_object.comparison()

# Getting record start date
def new_record_start_date(ld_detail_new):
    # Chrome driver setting
    service = Service(executable_path=ChromeDriverManager().install())
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('start-maximized')
    chrome_options.add_argument('disable-infobars')
    chrome_options.add_argument("--disable-extensions")
    driver = webdriver.Chrome(service=service, options=chrome_options)
    # Selenium for record start date
    for item in ld_detail_new:
        print(
            f"Record_start_link: https://www.londonstockexchange.com/stock/{item['Code']}/{item['Name_api']}/analysis")
        driver.get(f"https://www.londonstockexchange.com/stock/{item['Code']}/{item['Name_api']}/analysis")
        time.sleep(5)
        if len(driver.find_elements(By.ID, 'ccc-notify-accept')) > 0:
            driver.execute_script("arguments[0].click();", driver.find_element(By.ID, 'ccc-notify-accept'))
        if len(driver.find_elements(By.CSS_SELECTOR, '.ng-input')) == 0:
            item['Record_start'] = None
            continue
        else:
            driver.find_element(By.CSS_SELECTOR, '.ng-input').click()
            driver.find_elements(By.CSS_SELECTOR, '.ng-option')[-1].click()
            time.sleep(10)
            page_bar = driver.find_elements(By.CSS_SELECTOR, '.paginator')
            print(f"Multiple pages: {len(page_bar)}")
            if len(page_bar) > 0:
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(3)
                driver.execute_script("arguments[0].scrollIntoView();",driver.find_element(By.CSS_SELECTOR, '.paginator'))
                time.sleep(3)
                driver.find_elements(By.CSS_SELECTOR, '.page-number')[-1].click()
            time.sleep(3)
            record_start = driver.find_elements(By.CSS_SELECTOR, '.hide-on-portrait')
            record_start_date = f"{record_start[-2].text} {record_start[-1].text}"
            item['Record_start'] = record_start_date
    return ld_detail_new

# Input new company's information
def new_all_info(list_of_dict):
    list_of_dict = new_record_start_date(list_of_dict)
    result = []
    for item in list_of_dict:
        print(f"Name for backend api: {item['Name_api']}")
        response_code = requests.get(f"https://api.londonstockexchange.com/api/gw/lse/instruments/alldata/{item['Code']}")
        response_name = requests.get(f"https://api.londonstockexchange.com/api/v1/pages?path=issuer-profile&parameters=tidm%253D{item['Code']}%2526tab%253Dcompany-page%2526issuername%253D{item['Name_api']}")
        code_res = response_code.json()
        name_res = response_name.json()['components'][1]['content'][6]
        dictionary = {
            "CompanyID" : f"LSE{code_res['issuercode']}",
            "StockCode" : code_res['issuercode'],
            "CompanyName" : item['Name'].upper(),
            "CompanyType" : code_res['category'],
            "Market" : code_res['market'],
            "IssueDate" : name_res['value']['starttradabledate'],
            "ISIN" : code_res['isin'],
            "URL" : f"https://www.londonstockexchange.com/stock/{code_res['issuercode']}/{item['Name_api']}",
            "CompanyDescription" : code_res['description'],
            "Website": name_res['value']['webaddress'],
            "Address" : name_res['value']['address'],
            "Industry" : name_res['value']['icbindustry'],
            "Supersector" : name_res['value']['icbsupersector'],
            "Sector" : name_res['value']['icbsector'],
            "Subsector" : name_res['value']['icbsubsector'],
            "DateTime_Update" : datetime.now(),
            "RecordStartDate" : item['Record_start'],
            "RecordEndDate" : code_res['datepreviousnews']
        }
        result.append(dictionary)
    df_detail_new = pd.DataFrame(result)
    df_detail_new.to_csv('./LSE_Detailed_New.csv', index=False)
    return result

# Input delisted company record end date
def delisted_lastest_record(list_of_dict):
    master = []
    for item in list_of_dict:
        response = requests.get(f"https://api.londonstockexchange.com/api/gw/lse/instruments/alldata/{item['Code']}")
        dictionary = {
        "CompanyID": response['issuercode'],
        "ISIN": response['isin'],
        "RecordEndDate": response['datepreviousnews'],
        "DateTime_Update": datetime.now()
        }
        master.append(dictionary)
    return master

new_all_info(ld_detail_new)

# https://www.londonstockexchange.com/news?tab=news-explorer&period=lastmonth&namecode=3i%20group%20plc
# https://api.londonstockexchange.com/api/gw/lse/instruments/alldata/III
# https://api.londonstockexchange.com/api/v1/pages?path=issuer-profile&parameters=tidm%253DIII%2526tab%253Dcompany-page%2526issuername%253D3i-group-plc