import json
import requests 
import pandas as pd

class LSE_code_isin:
    # Get code and isin from the internet
    def code_isin(self):
        link = {"get": "https://api.londonstockexchange.com/api/v1/pages?path=live-markets/market-data-dashboard/price-explorer&parameters=markets%253DMAINMARKET%252CAIM%252CPSM%252CATT%2526categories%253DEQUITY"}
        master = []
        i = 0
        while True:
            print(f"Scrapping page: {i}")
            if i == 0:
                response = requests.get(f"{link['get']}")
            else:
                response = requests.get(f"{link['get']}%2526page%253D{i}")
            list_dict = response.json()['components'][0]['content'][1]['value']['content']
            # No information for that page if it is the last one
            if len(list_dict) == 0:
                print(f"Last page:{link['get']}%2526page%253D{i}")
                break
            # main
            for elements in list_dict:
                name = []
                for letter in elements['issuername'].split(" "):
                    name.append(letter.strip("-").strip("\"").strip(",").strip("(").strip(")").strip(".").strip("&").strip(" ").strip("+"))
                name_api = ' '.join(name).replace("  ", " ").replace("\'", "-").replace("&", "-").replace(".", "-").replace(" ", "-").lower()
                dictionary = {
                    "Name": elements['issuername'],
                    "Code": elements['tidm'],
                    "ISIN": elements['isin'],
                    "Name_api" : name_api # Cleaned name for backend api
                }
                master.append(dictionary)
            i += 1

        df_scrapped = pd.DataFrame(master)
        df_scrapped.to_csv("./LSE_Scrapped.csv",index=False)
        df_table = pd.read_csv('LSE_Table.csv',usecols=['CompanyName', 'ISIN', 'Code'])

        return df_scrapped,df_table
    # Compare the datebase table's ISIN and scrapped ISIN => output new and delisted ISIN
    def comparison(self):
        df_scrapped,df_table = self.code_isin()
        # compare ISIN
        ld_scrapped = df_scrapped.T.to_dict().values()
        ld_table = df_table.T.to_dict().values()
        delisted = []
        new = []
        for item_scrapped in ld_scrapped:
            if item_scrapped['Code'] not in df_table['Code'].to_list() and item_scrapped['ISIN'] not in df_table['ISIN'].to_list():
                new.append(item_scrapped)
        for item_delisted in ld_table:
            if item_delisted['Code'] not in df_scrapped['Code'].to_list() and item_delisted['ISIN'] not in df_scrapped['ISIN'].to_list():
                delisted.append(item_delisted)

        # new/delisted ISIN left join scrapped/db_table getting more information
        df_delisted = pd.DataFrame(delisted, columns=['ISIN', 'Code'])
        df_new = pd.DataFrame(new, columns=['ISIN', 'Code'])
        df_detail_delisted = pd.merge(df_delisted, df_table, left_on=['ISIN', 'Code'], right_on=['ISIN', 'Code'],how='left')
        df_detail_new = pd.merge(df_new, df_scrapped, left_on=['ISIN', 'Code'], right_on=['ISIN', 'Code'], how='left')
        df_detail_new.to_csv('./LSE_New.csv', index=False)
        df_detail_delisted.to_csv('./LSE_Delisted.csv', index=False)
        # return as list of dict
        return df_detail_delisted.T.to_dict().values(),df_detail_new.T.to_dict().values()


