from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import pandas as pd
from main import *

# main_object = LSE_code_isin()
# ld_detail_delisted,ld_detail_new = main_object.comparison()
ld_detail_new = pd.read_csv('LSE_New.csv').T.to_dict().values()
# Chrome driver setting
service = Service(executable_path=ChromeDriverManager().install())
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--start-maximized')
chrome_options.add_argument('disable-infobars')
chrome_options.add_argument("--disable-extensions")
driver = webdriver.Chrome(service=service, options=chrome_options)
# Selenium for record start date
for item in ld_detail_new:
    print(f"Record_start_link: https://www.londonstockexchange.com/stock/{item['Code']}/{item['Name_api']}/analysis")
    driver.get(f"https://www.londonstockexchange.com/stock/{item['Code']}/{item['Name_api']}/analysis")
    time.sleep(5)
    if len(driver.find_elements(By.ID,'ccc-notify-accept')) > 0:
        driver.execute_script("arguments[0].click();",driver.find_element(By.ID,'ccc-notify-accept'))
    if len(driver.find_elements(By.CSS_SELECTOR, '.ng-input')) == 0:
        item['Record_start'] = None
        continue
    else:
        driver.find_element(By.CSS_SELECTOR, '.ng-input').click()
        driver.find_elements(By.CSS_SELECTOR, '.ng-option')[-1].click()
        time.sleep(10)
        page_bar = driver.find_elements(By.CSS_SELECTOR, '.paginator')
        print(f"Multiple pages: {len(page_bar)}")
        if len(page_bar) > 0:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(3)
            driver.execute_script("arguments[0].scrollIntoView();", driver.find_element(By.CSS_SELECTOR, '.paginator'))
            time.sleep(3)
            driver.find_elements(By.CSS_SELECTOR, '.page-number')[-1].click()
        time.sleep(3)
        record_start = driver.find_elements(By.CSS_SELECTOR, '.hide-on-portrait')
        record_start_date = f"{record_start[-2].text} {record_start[-1].text}"
        item['Record_start'] = record_start_date

